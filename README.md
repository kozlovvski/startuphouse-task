# JS Recruitment Task

## Description

We would like you to create an application that will display list of news fetched from The Guardian. You should use their API, which can be found here: [https://open-platform.theguardian.com/](https://open-platform.theguardian.com/)

Goal of this task is to check your JavaScript knowledge so please don't use any additional libraries. There are some exceptions, though. You can use `fetch` for http requests and if you are going to write some tests, you can of course use tools like `testing-library` or `jest`.

**Important**

- Please treat this tasks as something that would be shown to our customer, so focus on quality and best practices (and we don't mean only from the code point of view :) ).
- Also feel free to update or customize starter config. For example you can change `prettier` or `eslint` config or add something else that you are used to use on daily basis.
- We have provided sample html + css styling, so you can focus on writing JS code but you can change default layout if you want.

## Requirements

- Display list of news from last 30 days
- Add pagination: 10 items per page
- Add news filtering based on section selection from dropdown. You can restrict it only to: `sport`, `books`, `business`, `culture`
- Add search functionality for filtering news content based on provided phrase
- Each news list element should have following elements:
  - Title
  - Section name
  - Date of publication
  - Link to full article (open in new window)
  - "Read Later" button
- Clicking "Read later" button should add selected news to the "Read later" section on the right. Those elements should be stored somewhere and displayed even after refresh.
- Each element from "read later" can be removed by clicking "delete" button
- (Bonus) It would be beneficial if you would write some kind of tests, either unit or integration
- (Bonus) If you will find time, please briefly describe your approach to solving this task.

## Tools used

In order to keep things simple we used only really small number of libs for this boilerplate:

- [Parcel](https://en.parceljs.org) as a bundler
- [Milligram](https://milligram.io/) and [Normalize](https://necolas.github.io/normalize.css/) for some simple styling
- [Eslint](https://eslint.org/) and [Prettier](https://prettier.io/) for static code check
- [PostCSS](https://postcss.org/) with [Autoprefixr](https://autoprefixer.github.io/) for css compatibility

## My approach

First of all, I am not familiar with Parcel as I use Webpack on a daily basis, so I had to dive a little into the documentation and read how I can setup necessary tools. Mostly I wanted to use TypeScript, so it was a pleasant surprise, that it basically works out of the box. After getting to know Parcel a little better, I've set up preferred dependencies:

- I've installed changelog and commit dependencies like [standard-version](https://www.npmjs.com/package/standard-version)
- I've also configured SCSS (I didn't use it much, but I didn't know that would happen at the time I was preparing the environment)
- I've changed eslint and prettier config to match my preferrences and to include new dependencies like TypeScript or SCSS (I also fixed a bug in template where eslint would override prettier changes)
- I've updated lint-staged scripts to include new file extensions
- I've release a first (0.1.0) version

Then I was ready to actually start coding. I've spent a while thinking how I need to pass data between parts of the application - I am using React with functional components and hooks and Redux on a daily basis and I was trying to find a way to implement a similar approach, but I failed to do so. Due to the challenge time restrictions I decided on the first thing that I was sure would work. I opted for using a root class which would store application state and pass it's properties and methods to the other components, so that they could communicate in both directions. Hence I tried to split my code into simple, pure functions that would e.g. setup event listeners for a button and I passed a root class reference as an argument when it was needed (for example when button needed to change the state of the application).

Then it was pretty straightforward - I wrote components handling the filters, displaying the articles in the correct lists and I started to refactor the code when it got messy. After I had made a first version satisfying the challenge requirements, I adjusted the responsiveness (mainly by creating a drawer for read later articles on mobile devices) and I implemented a feature of my own - saving filtering parameters to the application URL and reading it on application load. Therefore when user e.g. copies a link to the articles in sport section and enters the application in the future, the app will automatically make a correct request based on params in the URL.

I also tried to support edge cases - for example I added information when either of the lists was empty to improve user experience. I also added an information when fetch request failed - either due to some bug on Guardian's part, or for example by providing incorrect params in the URL.

At last I created unit tests for some parts of the code - I wanted to do it first but I was worried that I wouldn't manage to write the whole application in time. However, I had slight issues - I am familiar with unit tests in React & Enzyme, but I didn't test plain JS applications. Therefore I encountered some issues and one of them was significant - I had a problem with one of my helper functions `getById` which basically returned `document.getElementById` call with provided id and if element was null, it called `console.warn` (I created this function mainly to ca tch typos in selectors). The problem was that in the actual app this function worked fine, but in the jest tests it always returned null. I think this issue is somehow related to `jsdom` used by jest, but I didn't manage to resolve that in time. This basically prevented me from creating most of the tests - in several places in the app I check wheter selected component is truthy before I use it. As jest always thought these Elements are nullish, it never accessed blocks in if statements or optionally chained properties.

If I had to do this challenge once more, I would definitely change some things: first of all I would setup SCSS modules with Parcel and split individual components styles into separate files and include them in appropriate component's folder (I like to use folder-by-feature structure in my React projects and contain all of the component's files in one folder). I would also move components HTML into the TS file so that everything would be in one place (right now some TS files return HTML adjusted to the data in provided arguments and some files only attach event listeners to the elements already existing in DOM).
