export default {
  id: 'culture/2020/nov/29/lesley-manville-i-was-always-quite-savvy',
  type: 'article',
  sectionId: 'culture',
  sectionName: 'Culture',
  webPublicationDate: '2020-11-29T10:00:25Z',
  webTitle: 'Lesley Manville: ‘I was always quite savvy’',
  webUrl:
    'https://www.theguardian.com/culture/2020/nov/29/lesley-manville-i-was-always-quite-savvy',
  apiUrl:
    'https://content.guardianapis.com/culture/2020/nov/29/lesley-manville-i-was-always-quite-savvy',
  isHosted: false,
  pillarId: 'pillar/arts',
  pillarName: 'Arts',
};
