import ArticlesController from './ArticlesController';
import './styles/main.scss';
import 'regenerator-runtime/runtime'; // https://flaviocopes.com/parcel-regeneratorruntime-not-defined/

// Please use https://open-platform.theguardian.com/documentation/

new ArticlesController();
