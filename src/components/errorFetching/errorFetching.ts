/**
 * A function that returns a string with an error displayed when fetch rejects
 */
const errorFetching = () => `
  <div class="error-fetching" data-testid="error-fetching">
    <h4>Whoops! Something went wrong. Please try again later.</h4>
  </div>
  `;

export default errorFetching;
