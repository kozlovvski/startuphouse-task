import { getByTestId } from '@testing-library/dom';

import errorFetching from './errorFetching';

describe('errorFetching', () => {
  let container: HTMLDivElement;

  beforeEach(() => {
    container = document.createElement('div');
    container.innerHTML = errorFetching();
  });

  test('should return a string', () => {
    expect(typeof errorFetching()).toBe('string');
  });

  test('should render a div', () => {
    expect(
      getByTestId(container, 'error-fetching') instanceof HTMLDivElement
    ).toBe(true);
  });

  test('should render a h4', () => {
    expect(container.querySelectorAll('h4').length).toBe(1);
  });
});
