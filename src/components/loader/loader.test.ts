import { getByTestId } from '@testing-library/dom';

import loader from './loader';

describe('loader', () => {
  let container: HTMLDivElement;

  beforeEach(() => {
    container = document.createElement('div');
    container.innerHTML = loader();
  });

  test('should return a string', () => {
    expect(typeof loader()).toBe('string');
  });

  test('should render a div', () => {
    expect(getByTestId(container, 'loader') instanceof HTMLDivElement).toBe(
      true
    );
  });

  test('should render a svg', () => {
    expect(container.querySelectorAll('svg').length).toBe(1);
  });
});
