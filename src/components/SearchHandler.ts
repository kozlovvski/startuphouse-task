import ArticlesController from '../ArticlesController';
import debounce from '../helpers/debounce/debounce';
import getById from '../helpers/getById';

const searchBar = getById<HTMLInputElement>('newsContentSearch');

/**
 * This class makes search input interactive.
 * It fires a new fetch when user inputs a string
 * TODO: change this into a pure function
 *
 * @class SearchHandler
 */
class SearchHandler {
  constructor(articlesController: ArticlesController) {
    if (searchBar) {
      searchBar.value = articlesController.searchValue;
      searchBar.addEventListener(
        'input',
        debounce((e) => {
          const value = (e.target as HTMLSelectElement).value;
          articlesController.searchValue = value;
          articlesController.updateArticles(true);
        }, 300)
      );
    }
  }
}

export default SearchHandler;
