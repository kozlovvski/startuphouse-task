import ArticlesController from '../ArticlesController';
import getById from '../helpers/getById';

const select = getById<HTMLSelectElement>('activePageSelect');

/**
 * This class makes pages select interactive.
 * It fires a new fetch when page number is changed.
 * It also handles pages select length updating.
 *
 * @class ActivePageSelectHandler
 */
class ActivePageSelectHandler {
  private controller;

  constructor(articlesController: ArticlesController) {
    this.controller = articlesController;
    if (select) {
      select.value = articlesController.page;
      select.addEventListener('change', (e) => {
        const value = (e.target as HTMLSelectElement).value;
        articlesController.page = value;
        articlesController.updateArticles();
      });
    }
  }

  update = (pages: number, resetPage?: boolean) => {
    if (select) {
      select.innerHTML = '';

      for (let i = 1; i <= pages; i++) {
        const option = document.createElement('option');
        option.value = String(i);
        option.text = String(i);
        select.appendChild(option);
      }

      if (resetPage) {
        this.controller.page = '1';
        select.value = '1';
      } else {
        select.value = this.controller.page;
      }
    }
  };
}

export default ActivePageSelectHandler;
