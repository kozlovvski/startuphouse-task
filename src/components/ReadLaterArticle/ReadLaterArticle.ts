import ArticlesController from '../../ArticlesController';

/**
 * A function that renders a read later article card
 */
const ReadLaterArticle = (
  { webTitle, webUrl, id }: ArticleData,
  articleController: ArticlesController
) => {
  const template = `
    <h4 class="readLaterItem-title">${webTitle}</h4>
    <section>
      <a href="${webUrl}" class="button button-outline" target="_blank" rel="norefferer">Read</a>
      <button class="button button-clear">Remove</button>
    </section>
  `;

  const newElement = document.createElement('li');
  newElement.id = 'read-later-' + id;
  newElement.classList.add('readLaterItem');
  newElement.innerHTML = template;

  newElement.querySelector('button')?.addEventListener('click', () => {
    articleController.removeFromReadLater(id);
  });

  return newElement;
};

export default ReadLaterArticle;
