import ArticlesController from '../../ArticlesController';
import fakeArticle from '../../test/fakeArticle';
import ReadLaterArticle from './ReadLaterArticle';

describe('ReadLaterArticle', () => {
  let container: HTMLLIElement;

  beforeEach(() => {
    jest.spyOn(global.console, 'warn').mockImplementation(() => {});
    const articlesController = new ArticlesController();
    container = ReadLaterArticle(fakeArticle, articlesController);
  });

  test('should be a function', () => {
    expect(typeof ReadLaterArticle).toBe('function');
  });

  test('should return a li element', () => {
    expect(container instanceof HTMLLIElement).toBe(true);
  });
});
