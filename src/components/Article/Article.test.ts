import ArticlesController from '../../ArticlesController';
import fakeArticle from '../../test/fakeArticle';
import Article from './Article';

describe('Article', () => {
  let container: HTMLLIElement;

  beforeEach(() => {
    jest.spyOn(global.console, 'warn').mockImplementation(() => {});
    const articlesController = new ArticlesController();
    container = Article(fakeArticle, articlesController);
  });

  test('should be a function', () => {
    expect(typeof Article).toBe('function');
  });

  test('should return a li element', () => {
    expect(container instanceof HTMLLIElement).toBe(true);
  });
});
