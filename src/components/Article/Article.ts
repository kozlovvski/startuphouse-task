import ArticlesController from '../../ArticlesController';

/**
 * A function that renders an article card
 */
const Article = (
  articleData: ArticleData,
  articleController: ArticlesController
) => {
  const { webTitle, webUrl, sectionName, webPublicationDate, id } = articleData;

  const template = `
    <article class="news">
      <header>
        <h3>${webTitle}</h3>
      </header>
      <section class="newsDetails">
        <ul>
          <li><strong>Section Name: </strong>${sectionName}</li>
          <li><strong>Publication Date: </strong>${new Date(
            webPublicationDate
          ).toLocaleDateString()}</li>
        </ul>
      </section>
      <section class="newsActions">
        <a href="${webUrl}" class="button" target="_blank" rel="norefferer">Full article</a>
        <button class="button button-outline">Read Later</button>
      </section>
    </article>
  `;

  const newElement = document.createElement('li');
  newElement.id = id;
  newElement.innerHTML = template;

  newElement.querySelector('button')?.addEventListener('click', () => {
    articleController.addToReadLater(articleData);
  });

  return newElement;
};

export default Article;
