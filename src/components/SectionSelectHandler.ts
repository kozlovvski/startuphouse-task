import ArticlesController from '../ArticlesController';
import getById from '../helpers/getById';

const select = getById<HTMLSelectElement>('sectionSelect');

class SectionSelectHandler {
  constructor(articlesController: ArticlesController) {
    if (select) {
      select.value = articlesController.section;
      select.addEventListener('change', (e) => {
        const value = (e.target as HTMLSelectElement).value;
        articlesController.section = value;
        articlesController.updateArticles(true);
      });
    }
  }
}

export default SectionSelectHandler;
