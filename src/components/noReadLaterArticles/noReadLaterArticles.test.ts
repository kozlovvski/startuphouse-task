import { getByTestId } from '@testing-library/dom';

import noReadLaterArticles from './noReadLaterArticles';

describe('noReadLaterArticles', () => {
  let container: HTMLDivElement;

  beforeEach(() => {
    container = document.createElement('div');
    container.innerHTML = noReadLaterArticles();
  });

  test('should return a string', () => {
    expect(typeof noReadLaterArticles()).toBe('string');
  });

  test('should render a div', () => {
    expect(
      getByTestId(container, 'no-read-later-articles') instanceof HTMLDivElement
    ).toBe(true);
  });

  test('should render a h4', () => {
    expect(container.querySelectorAll('h4').length).toBe(1);
  });

  test('should render a p', () => {
    expect(container.querySelectorAll('p').length).toBe(1);
  });
});
