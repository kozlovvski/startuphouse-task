/**
 * A function that returns a string with no read later articles display
 */
const noReadLaterArticles = () => `
  <div class="no-read-later-articles" data-testid="no-read-later-articles">
    <h4>Currently your list is empty.</h4>
    <p>When you save an article for later, it will be displayed here.</p>
  </div>
  `;

export default noReadLaterArticles;
