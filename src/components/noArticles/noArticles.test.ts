import { getByTestId } from '@testing-library/dom';

import noArticles from './noArticles';

describe('noArticles', () => {
  let container: HTMLDivElement;

  beforeEach(() => {
    container = document.createElement('div');
    container.innerHTML = noArticles();
  });

  test('should return a string', () => {
    expect(typeof noArticles()).toBe('string');
  });

  test('should render a div', () => {
    expect(
      getByTestId(container, 'no-articles') instanceof HTMLDivElement
    ).toBe(true);
  });

  test('should render a h4', () => {
    expect(container.querySelectorAll('h4').length).toBe(1);
  });
});
