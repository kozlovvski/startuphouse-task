/**
 * A function that returns a string with no articles display
 */
const noArticles = () => `
  <div class="no-articles" data-testid="no-articles">
    <h4>Whoops! It looks like there are no articles matching your request.</h4>
  </div>
  `;

export default noArticles;
