import ActivePageSelectHandler from './components/ActivePageSelectHandler';
import Article from './components/Article/Article';
import errorFetching from './components/errorFetching/errorFetching';
import loader from './components/loader/loader';
import noArticles from './components/noArticles/noArticles';
import noReadLaterArticles from './components/noReadLaterArticles/noReadLaterArticles';
import ReadLaterArticle from './components/ReadLaterArticle/ReadLaterArticle';
import fetchArticles from './helpers/fetchArticles';
import getById from './helpers/getById';
import setReadLaterArticles from './helpers/setReadLaterArticles';
import fireHandlers from './util';

const readLaterList = getById('readLaterList');
const newsList = getById('newsList');

/**
 * A main app component. It stores query parameters and handles article lists updating
 *
 * @class ArticlesController
 */
export default class ArticlesController {
  articles: ArticleData[] = [];
  activePageSelectHandler: ActivePageSelectHandler;
  readLaterArticles: ArticleData[] = [];
  page = '1';
  section = 'all';
  searchValue = '';

  constructor() {
    fireHandlers(this);

    this.updateArticles();
    this.renderReadLaterArticles();

    this.activePageSelectHandler = new ActivePageSelectHandler(this);
  }

  /**
   * This function fetches Guardian articles based on selected filters
   * and displays correct components based on results
   *
   * @memberof ArticlesController
   */
  updateArticles = async (resetPage?: boolean) => {
    if (newsList) {
      // Show a loader during request
      newsList.innerHTML = loader();
      try {
        const { results, pages } = await fetchArticles({
          page: resetPage ? '1' : this.page,
          section: this.section,
          searchValue: this.searchValue,
        });
        this.articles = results;
        this.activePageSelectHandler.update(pages, resetPage);

        if (results.length) {
          newsList.innerHTML = '';
          results.forEach((a) => {
            newsList.appendChild(Article(a, this));
          });
        } else {
          // When list is empty, render an appropriate component
          newsList.innerHTML = noArticles();
        }
      } catch (error) {
        // When promise rejects, render an error component
        newsList.innerHTML = errorFetching();
      }
    }
  };

  /**
   * This function takes a list of read later articles and displays appropriate components
   *
   * @memberof ArticlesController
   */
  renderReadLaterArticles = (articles = this.readLaterArticles) => {
    if (readLaterList) {
      if (articles.length) {
        readLaterList.innerHTML = '';
        articles.forEach((a) => {
          readLaterList.appendChild(ReadLaterArticle(a, this));
        });
      } else {
        // When list is empty, render an appropriate component
        readLaterList.innerHTML = noReadLaterArticles();
      }
    }
  };

  /**
   * This function handles updating internal read later articles list,
   * saves read later articles to localStorage and fires a rerender
   *
   * @memberof ArticlesController
   */
  updateReadLaterArticles = (newArticles: ArticleData[]) => {
    this.readLaterArticles = newArticles;
    setReadLaterArticles(newArticles);
    this.renderReadLaterArticles();
  };

  /**
   * This function adding a new read later article
   *
   * @memberof ArticlesController
   */
  addToReadLater = (articleData: ArticleData) => {
    if (!this.readLaterArticles.some((a) => a.id === articleData.id)) {
      const newArticles = [...this.readLaterArticles, articleData];
      this.updateReadLaterArticles(newArticles);
    }
  };

  /**
   * This function adding removing a read later article
   *
   * @memberof ArticlesController
   */
  removeFromReadLater = (id: string) => {
    const newArticles = this.readLaterArticles.filter((a) => a.id !== id);
    this.updateReadLaterArticles(newArticles);
  };
}
