import ArticlesController from '../ArticlesController';

/**
 * Gets data stored in `localStorage` under `readLaterArticles` key
 * and assigns it to the appropriate field in ArticlesController
 */
const getReadLaterArticles = (articleController: ArticlesController): void => {
  const articlesString = localStorage.getItem('readLaterArticles');
  articleController.readLaterArticles = articlesString
    ? JSON.parse(articlesString)
    : [];
};

export default getReadLaterArticles;
