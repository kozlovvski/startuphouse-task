import ArticlesController from '../ArticlesController';
import SectionSelectHandler from '../components/SectionSelectHandler';
import SearchHandler from '../components/SearchHandler';
import getReadLaterArticles from './getReadLaterArticles';
import getURLParams from './getURLParams';
import readLaterButtonHandler from './readLaterButtonHandler';

/**
 * This function fires all necessary handlers, event listeners etc.
 * It's main purpose is to keep ArticlesController clean
 */
const fireHandlers = (articlesController: ArticlesController) => {
  getURLParams(articlesController);
  getReadLaterArticles(articlesController);
  readLaterButtonHandler();

  new SectionSelectHandler(articlesController);
  new SearchHandler(articlesController);
};

export default fireHandlers;
