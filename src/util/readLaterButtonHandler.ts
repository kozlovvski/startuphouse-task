import getById from '../helpers/getById';

/**
 * A function which handles read later list displaying
 * on mobile devices. Attaches all necessary event handlers
 * and adds classes necessary for a transition
 */
const readLaterButtonHandler = () => {
  const button = getById<HTMLButtonElement>('readLaterButton');
  const readLaterContainer = getById('readLaterContainer');
  const backdrop = getById('backdrop');

  if (button && readLaterContainer && backdrop) {
    button.addEventListener('click', () => {
      document.body.classList.add('lock-scroll');
      readLaterContainer.classList.add('active');

      window.requestAnimationFrame(() => {
        backdrop.classList.remove('backdrop-exit-done');
        backdrop.classList.add('backdrop-enter');
        window.requestAnimationFrame(() => {
          backdrop.classList.add('backdrop-enter-active');
          backdrop.classList.remove('backdrop-enter');
        });
      });
      setTimeout(() => {
        backdrop.classList.add('backdrop-enter-done');
        backdrop.classList.remove('backdrop-enter-active');
      }, 300);
    });

    backdrop.addEventListener('click', () => {
      document.body.classList.remove('lock-scroll');
      readLaterContainer.classList.remove('active');

      window.requestAnimationFrame(() => {
        backdrop.classList.add('backdrop-exit');
        backdrop.classList.remove('backdrop-enter-done');
        window.requestAnimationFrame(() => {
          backdrop.classList.add('backdrop-exit-active');
          backdrop.classList.remove('backdrop-exit');
        });
      });
      setTimeout(() => {
        backdrop.classList.add('backdrop-exit-done');
        backdrop.classList.remove('backdrop-exit-active');
      }, 300);
    });
  }
};

export default readLaterButtonHandler;
