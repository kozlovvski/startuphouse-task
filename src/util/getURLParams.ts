import ArticlesController from '../ArticlesController';

/**
 * A function which gets params from URL and assigns them
 * to correct fields in ArticlesController
 */
const getURLParams = (articlesController: ArticlesController) => {
  const params = new URLSearchParams(window.location.search);

  (['page', 'section', 'searchValue'] as Array<
    keyof ArticlesController
  >).forEach((k) => {
    const paramValue = params.get(k);
    if (paramValue) {
      articlesController[k] = paramValue as any; // TODO: find a way to get rid of this warning
    }
  });
};

export default getURLParams;
