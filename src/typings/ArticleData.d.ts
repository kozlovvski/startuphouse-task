/**
 * Data type of an incoming Guardian article
 */
type ArticleData = {
  apiUrl: string;
  id: string;
  isHosted: boolean;
  pillarId: string;
  pillarName: string;
  sectionId: string;
  sectionName: string;
  type: string;
  webPublicationDate: string;
  webTitle: string;
  webUrl: string;
};

/**
 * Response type for a Guardian articles fetch
 */
type FetchArticleDataResponse = {
  response: {
    currentPage: number;
    orderBy: string;
    pageSize: number;
    pages: number;
    results: ArticleData[];
    startIndex: number;
    status: string;
    total: number;
    userTier: string;
  };
};
