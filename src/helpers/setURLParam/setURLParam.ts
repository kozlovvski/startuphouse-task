import updateHistory from '../updateHistory/updateHistory';

/**
 * A function which sets a param in URL
 */
const setURLParam = (name: string, value: string) => {
  const params = new URLSearchParams(window.location.search);
  params.set(name, value);

  updateHistory(params);
};

export default setURLParam;
