import { waitFor } from '@testing-library/dom';
import setURLParam from './setURLParam';

describe('setURLParam', () => {
  test('should add a param', () => {
    const oldParams = 'section=culture';
    window.history.replaceState(undefined, '', '/?' + oldParams);

    setURLParam('page', '8');

    waitFor(() => {
      expect(window.location.search).toBe('section=culture&page=8');
    });
  });

  test('should update a param', () => {
    const oldParams = 'section=culture&test=11';
    window.history.replaceState(undefined, '', '/?' + oldParams);

    setURLParam('test', 'whoops');

    waitFor(() => {
      expect(window.location.search).toBe('section=culture&test=whoops');
    });
  });
});
