import { waitFor } from '@testing-library/dom';

import updateHistory from './updateHistory';

describe('updateHistory', () => {
  test('should set correct params', () => {
    const params = new URLSearchParams('section=culture');
    updateHistory(params);

    waitFor(() => {
      expect(window.location.search).toBe('section=culture');
    });
  });
});
