/**
 * A function which updates window.location based on provided params
 */
const updateHistory = (params: URLSearchParams) => {
  window.history.replaceState(
    undefined,
    'Guardian Articles search engine',
    decodeURIComponent(
      `${window.location.pathname}${params.toString() ? '?' + params : ''}`
    )
  );
};

export default updateHistory;
