/**
 * A function which gets a HTMLElement by id and shows a console warning if element is not found
 */
const getById = <T extends HTMLElement>(id: string) => {
  const element = document.getElementById(id) as T;

  if (element) {
    return element;
  } else {
    console.warn(`Element with id: ${id} is undefined`);
  }
};

export default getById;
