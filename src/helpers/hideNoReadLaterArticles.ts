/**
 * A function that deleted a noReadLaterArticles component
 */
const hideNoReadLaterArticles = () => {
  document.querySelector('.no-read-later-articles')?.remove();
};

export default hideNoReadLaterArticles;
