import deleteURLParam from './deleteURLParam/deleteURLParam';
import get30DaysAgoISODate from './get30DaysAgoISODate';
import setURLParam from './setURLParam/setURLParam';

type FetchArticlesParams = {
  page: string;
  section: string;
  searchValue: string;
};

/**
 * A function which fetches and returns guardian articles based on provided params
 */
const fetchArticles = ({ page, section, searchValue }: FetchArticlesParams) => {
  const baseUrl = 'https://content.guardianapis.com/search';
  const apiKey = process.env.API_KEY;

  section !== 'all'
    ? setURLParam('section', section)
    : deleteURLParam('section');
  page !== '1' ? setURLParam('page', page) : deleteURLParam('page');
  searchValue
    ? setURLParam('searchValue', searchValue)
    : deleteURLParam('searchValue');

  return fetch(
    `${baseUrl}?from-date=${get30DaysAgoISODate()}&page=${page}${
      section !== 'all' ? '&section=' + section : ''
    }${
      searchValue ? '&query-fields=body&q=' + encodeURI(searchValue) : ''
    }&api-key=${apiKey}`
  )
    .then((data) => data.json() as Promise<FetchArticleDataResponse>)
    .then((data) => data.response);
};

export default fetchArticles;
