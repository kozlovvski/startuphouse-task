/**
 * This function returns a date 30 days ago in YYYY-MM-DD format
 */

const get30DaysAgoISODate = () => {
  const thirtyDaysAgo = new Date(
    Date.now() -
      // current millis
      (30 * 24 * 60 - // 30 days in minutes
        new Date().getTimezoneOffset()) * // substract timezone offset as it will be lost when converting to UTC time
        60 *
        1000 // change minutes to millis so it can be correctly substracted from Date.now() result
  );

  return thirtyDaysAgo.toISOString().slice(0, 10); // YYYY-MM-DD
};

export default get30DaysAgoISODate;
