import debounce from './debounce';

jest.useFakeTimers();

describe('debounce', () => {
  const mockedCallback = jest.fn();
  afterEach(() => {
    jest.clearAllMocks();
    jest.clearAllTimers();
  });

  test('should return a function', () => {
    expect(typeof debounce(mockedCallback, 300)).toBe('function');
  });

  test('should call a provided function', () => {
    const debouncedFunc = debounce(mockedCallback, 300);

    debouncedFunc();

    setTimeout(() => {
      expect(mockedCallback).toBeCalled();
    }, 1000);
  });

  test('should call a setTimeout', () => {
    const debouncedFunc = debounce(mockedCallback, 300);

    debouncedFunc();

    setTimeout(() => {
      expect(setTimeout).toBeCalled();
    }, 1000);
  });

  test('should call a clearTimeout', () => {
    const debouncedFunc = debounce(mockedCallback, 300);

    debouncedFunc();

    setTimeout(() => {
      expect(clearTimeout).toBeCalled();
    }, 1000);
  });

  test('should call provided function once when multiple requests are sent', () => {
    const debouncedFunc = debounce(mockedCallback, 300);

    debouncedFunc();
    debouncedFunc();
    debouncedFunc();
    debouncedFunc();
    debouncedFunc();

    setTimeout(() => {
      expect(mockedCallback).toBeCalledTimes(1);
    }, 1000);
  });
});
