import updateHistory from '../updateHistory/updateHistory';

/**
 * A function which deletes a param from URL
 */
const deleteURLParam = (name: string) => {
  const params = new URLSearchParams(window.location.search);
  params.delete(name);

  updateHistory(params);
};

export default deleteURLParam;
