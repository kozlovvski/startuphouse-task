import { waitFor } from '@testing-library/dom';
import deleteURLParam from './deleteURLParam';

describe('deleteURLParam', () => {
  test('should delete a param from url', () => {
    const oldParams = 'section=culture&page=15';
    window.history.replaceState(undefined, '', '/?' + oldParams);

    deleteURLParam('page');

    waitFor(() => {
      expect(window.location.search).toBe('section=culture');
    });
  });
});
