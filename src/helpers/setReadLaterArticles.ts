/**
 * Saves provided articles to `localStorage` under `readLaterArticles` key
 */
const setReadLaterArticles = (articles: ArticleData[]) => {
  localStorage.setItem('readLaterArticles', JSON.stringify(articles));
};

export default setReadLaterArticles;
