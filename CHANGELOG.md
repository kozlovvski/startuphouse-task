# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.2](https://gitlab.com/kozlovvski/startuphouse-task/compare/v0.1.1...v0.1.2) (2020-12-29)

### Bug Fixes

- **index.html:** remove unnecessary boilerplate read later item ([9554cce](https://gitlab.com/kozlovvski/startuphouse-task/commit/9554cce7b892fe677d87cff73ca03a7ec31a252d))

### [0.1.1](https://gitlab.com/kozlovvski/startuphouse-task/compare/v0.1.0...v0.1.1) (2020-12-29)

### Features

- add error fetching component displayed when fetch rejects ([01194d6](https://gitlab.com/kozlovvski/startuphouse-task/commit/01194d6dbf73e9e6d345edbfdd9d9edf6068c951))
- add noReadLaterArticles component displayed when later read list is empty ([42333d9](https://gitlab.com/kozlovvski/startuphouse-task/commit/42333d9f5a494e9485a7cc718fed3356f96b86f1))
- **active-page:** add active page select controller ([81f2eaa](https://gitlab.com/kozlovvski/startuphouse-task/commit/81f2eaabdf2584385f41cfabcfd34e80efbd303d))
- **articles:** setup basic articles fetching and displaying ([ab89d80](https://gitlab.com/kozlovvski/startuphouse-task/commit/ab89d8063af31442031556d5edc167fcf4e9ed65))
- **loader:** add a loader during fetch request ([1baeae4](https://gitlab.com/kozlovvski/startuphouse-task/commit/1baeae462950fa248da32723131d14a09f6b4bbc))
- **no-articles:** add a no-articles component displayed when query results are empty ([335c62b](https://gitlab.com/kozlovvski/startuphouse-task/commit/335c62be899e480e23a604baf6d4b932542ec8d6))
- **params:** add saving to and reading query options from url params ([3d163f5](https://gitlab.com/kozlovvski/startuphouse-task/commit/3d163f5b6fa2feb5ffecf4bc837c54e1112c0155))
- **read-later:** add basic read later functionality ([d3e9817](https://gitlab.com/kozlovvski/startuphouse-task/commit/d3e9817a65b1dad114a65f9a3863d60fca4fe6e3))
- **read-later:** finish read later feature ([770729a](https://gitlab.com/kozlovvski/startuphouse-task/commit/770729a9724b56964bb550f239f8ce8b8c520ebf))
- **search-handler:** add search input handling ([f780002](https://gitlab.com/kozlovvski/startuphouse-task/commit/f780002fdb5fca3b15b5fc708daa1a6c0ebc3408))
- **section-select:** make section select interactive ([5685c6c](https://gitlab.com/kozlovvski/startuphouse-task/commit/5685c6c9ee51c995d3e1c9e0d006f0504de29036))

### Bug Fixes

- **history:** change history pushState to replaceState when changing params ([4ced486](https://gitlab.com/kozlovvski/startuphouse-task/commit/4ced486293c04fc8734df69344d701dcc3a1f897))
- **links:** add target and rel attributes in links ([e2d823f](https://gitlab.com/kozlovvski/startuphouse-task/commit/e2d823fb79cded1087ea047b7eaa985fbabf0a88))
- **params:** remove boilerplate page select option from index.html ([d55c3bd](https://gitlab.com/kozlovvski/startuphouse-task/commit/d55c3bd87f8c5a38d17de1402e5ae577b5543e90))
- **read-later:** prevent article from being added more than once to read later list ([be3bb79](https://gitlab.com/kozlovvski/startuphouse-task/commit/be3bb79c50629b211f5410298667cb75d82d27d3))
- **scss:** improve read later section styling ([aae8cfd](https://gitlab.com/kozlovvski/startuphouse-task/commit/aae8cfdf935905744f161887c5c3409c1b9c2158))
- **scss:** improve responsiveness, add read later articles drawer on mobile ([0100ae2](https://gitlab.com/kozlovvski/startuphouse-task/commit/0100ae2957f55a5ae1d9b91567075d13f7011ff6))

## 0.1.0 (2020-12-28)
